Class {
	#name : #MyLinkedListTest,
	#superclass : #TestCase,
	#instVars : [
		'l'
	],
	#category : #MyLinkedListTests
}

{ #category : #tests }
MyLinkedListTest >> setUp [

	super setUp.
	l := MyLinkedList initialize
]

{ #category : #tests }
MyLinkedListTest >> testAtCallsAtMethodOnItsHead [

	l
		push: 1;
		push: 2;
		push: 3.
	self assert: (l at: 2) equals: l head next.
	self should: [ l at: 4 ] raise: Error
]

{ #category : #tests }
MyLinkedListTest >> testAtPutChangesHeadWhenFirstIndexIs1 [

	| oldHead |
	l
		push: 1;
		push: 2;
		push: 3.
	oldHead := l head.
	self
		assert: (l
				 at: 2 put: 4;
				 yourself) head
		equals: oldHead.
	self
		assert: (l
				 at: 1 put: 0;
				 yourself) head next
		equals: oldHead
]

{ #category : #tests }
MyLinkedListTest >> testAtPutChangesLinkingCorrectly [

	| cellBefore cellAfter newCell |
	l
		push: 10;
		push: 4;
		push: 6;
		push: 3.
	cellBefore := l head next.
	cellAfter := cellBefore next.
	newCell := l at: 3 put: 9.
	self
		assert: newCell value equals: 9;
		assert: cellBefore next equals: newCell;
		assert: newCell next equals: cellAfter
]

{ #category : #tests }
MyLinkedListTest >> testAtPutChangesTailWhenIndexEqualsLengthOfTheListPlus1 [

	| oldTail |
	l := MyLinkedList initialize
		     push: 1;
		     push: 2;
		     push: 3;
		     yourself.
	oldTail := l tail.
	self
		assert: (l
				 at: 3 put: 4;
				 yourself) tail
		equals: oldTail.
	self
		assert: (l
				 at: 5 put: 5;
				 yourself) tail
		equals: oldTail next
]

{ #category : #tests }
MyLinkedListTest >> testAtPutThrowsErrorWhenIndexIsNegative [

	l := MyLinkedList initialize
		     push: 1;
		     push: 2;
		     push: 3;
		     yourself.
	self shouldnt: [ l at: 1 put: 0 ] raise: Error.
	self should: [ l at: 0 put: 4 ] raise: Error
]

{ #category : #tests }
MyLinkedListTest >> testAtPutThrowsErrorWhenIndexIsTooLarge [

	l := MyLinkedList initialize
		     push: 1;
		     push: 2;
		     push: 3.
	self shouldnt: [ l at: 4 put: 4 ] raise: Error.
	self should: [ l at: 6 put: 6 ] raise: Error
]

{ #category : #tests }
MyLinkedListTest >> testAtThrowsErrorWhenHeadIsNil [

	l push: 1.
	self shouldnt: [ l at: 1 ] raise: Error.
	self should: [ MyLinkedList initialize at: 1 ] raise: Error
]

{ #category : #tests }
MyLinkedListTest >> testContainsIsCorrect [
	l push: 4; push: 7; push: 8; push: 15.
	self assert: (l contains: 7); assert: (l contains: 19) not.
	l push: 19.
	self assert: (l contains: 19)
]

{ #category : #running }
MyLinkedListTest >> testInitializeCreatesEmptyLinkedList [

	self
		assert: l isEmpty;
		assert: l head isNil;
		assert: l tail isNil
]

{ #category : #tests }
MyLinkedListTest >> testLengthWorksProperly [

	self
		assert: l length equals: 0;
		assert: (l push: 10) length equals: 1;
		assert: (l push: 7) length equals: 2
]

{ #category : #tests }
MyLinkedListTest >> testPopCorrectsHeadAndLinking [

	| oldHead newHead |
	l
		push: 10;
		push: 8.
	newHead := l head next.
	oldHead := l head.
	self
		assert: l pop equals: oldHead;
		assert: l head equals: newHead;
		assert: newHead next isNil
]

{ #category : #tests }
MyLinkedListTest >> testPopCorrectsHeadWhenOldHeadHasNoNextCell [

	l
		push: 5;
		pop.
	self assert: l head isNil & l tail isNil
]

{ #category : #tests }
MyLinkedListTest >> testPopThrowsErrorWhenEmptyTest [
	self should: [ MyLinkedList initialize pop ] raise: Error
]

{ #category : #tests }
MyLinkedListTest >> testPushChangesHeadAndTailWhenListIsEmpty [

	| c |
	l push: 10.
	self
		assert: l head equals: l tail;
		assert: l head value equals: 10;
		assert: l head next isNil
]

{ #category : #tests }
MyLinkedListTest >> testPushChangesHeadWithoutModifyingTailWhenListIsNotEmptyTest [

	| c |
	l := MyLinkedList initialize push: 8.
	c := l head.
	l push: 6.
	self
		assert: l head value equals: 6;
		assert: l head next equals: c;
		assert: l tail equals: c
]

{ #category : #tests }
MyLinkedListTest >> testRemoveAtChangesHeadWhenNecessaryTest [
	|oldHead newHead|
	l push: 1; push: 2; push: 3; push: 4.
	oldHead := l head.
	self assert: (l removeAt: 2; yourself) head equals: oldHead.
	newHead := oldHead next.
	self assert: (l removeAt: 1; yourself) head equals: newHead
]

{ #category : #tests }
MyLinkedListTest >> testRemoveAtChangesTailWhenNecessaryTest [
	|oldTail newTail|
	l push: 1; push: 2; push: 3; push: 4.
	oldTail := l tail.
	self assert: (l removeAt: 3; yourself) tail equals: oldTail.
	newTail := l head next .
	self assert: (l removeAt: 3; yourself) tail equals: newTail
]

{ #category : #tests }
MyLinkedListTest >> testRemoveAtCorrectsLinking [
	|cellBefore cellAfter removedCell|
	l push: 1; push: 2; push: 3; push: 4.
	cellBefore := l head. 
	cellAfter := cellBefore next next.
	removedCell := l removeAt: 2.
	self assert: cellBefore next equals: cellAfter
]

{ #category : #tests }
MyLinkedListTest >> testRemoveAtReturnsTheCellAtRightIndexUnlinked [
	|removedCell|
	l push: 1; push: 2; push: 3; push: 4; push: 5.
	removedCell := l at: 4.
	self assert: (l removeAt: 4 ) equals: removedCell.
	self assert: removedCell next isNil
]

{ #category : #tests }
MyLinkedListTest >> testRemoveAtThrowsErrorWhenIndexIsGreaterThanTheLengthOfTheList [
	l push: 1; push: 2; push: 3; push: 4; push: 5.
	self shouldnt: [ l removeAt: 5 ] raise: Error.
	self should: [ l removeAt: 5 ] raise: Error
]

{ #category : #tests }
MyLinkedListTest >> testRemoveAtThrowsErrorWhenIndexIsNegative [
	l push: 1; push: 2.
	self shouldnt: [ l removeAt: 1 ] raise: Error.
	self should: [ l removeAt: 0 ] raise: Error
]
