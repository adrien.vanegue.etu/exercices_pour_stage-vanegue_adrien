Class {
	#name : #CellTest,
	#superclass : #TestCase,
	#category : #MyLinkedListTests
}

{ #category : #tests }
CellTest >> testANewCellHasLength1 [
	self assert: Cell new length equals: 1
]

{ #category : #tests }
CellTest >> testAtReturnsCellAtRightIndex [
	|c|
	c := Cell withValue: 9 withNext: (Cell withValue: 5 withNext: (Cell withValue: 4 withNext: nil)).
	self assert: (c at: 1) value equals: 9; assert: (c at: 2) value equals: 5; 
			assert: (c at: 3) value equals: 4
]

{ #category : #tests }
CellTest >> testAtThrowsErrorWhenIndexIsNegative [
	|c|
	c := Cell withValue: 8 withNext: nil.
	self shouldnt: [c at: 1]  raise: Error.
	self should: [c at: 0 ] raise: Error
]

{ #category : #tests }
CellTest >> testAtThrowsExceptionWhenIndexIsTooLarge [

	| c |
	c := Cell
		     withValue: 1
		     withNext:
		     (Cell withValue: 2 withNext: (Cell withValue: 3 withNext: nil)).
	self shouldnt: [c at: 3] raise: Error.
	self should: [ c at: 4 ] raise: Error
]

{ #category : #tests }
CellTest >> testCellIsIntializedCorrectly [
	|c|
	c := Cell withValue: 2 withNext: nil.
	self assert: c value equals: 2; assert: c next isNil
]

{ #category : #tests }
CellTest >> testLengthIncrementsWithEachNewLinkTest [ 
	|c|
	c := Cell new. c next: Cell new.
	self assert: c length equals: 2
]

{ #category : #tests }
CellTest >> testNextCellIsSetCorrectly [
	|c c2|
	c := Cell new.
	c2 := Cell new.
	self assert: (c next: c2) equals: c2.
	self assert: c next equals: c2
]

{ #category : #tests }
CellTest >> testSearchIsCorrect [
	|c|
	c := Cell withValue: 10 withNext: (Cell withValue: 4 withNext: (Cell withValue: 2 withNext: nil)).
	self assert: (c contains: 4); assert: (c contains: 6) not.
	c next next next: (Cell withValue: 6 withNext: nil).
	self assert: (c contains: 6)
]

{ #category : #running }
CellTest >> testValueIsSetCorrectly [
	|c|
	c := Cell new.
	self assert: (c value: 5) equals: 5.
	self assert: c value equals: 5
	
]
