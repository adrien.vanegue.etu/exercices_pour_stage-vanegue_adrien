"
Class part:  I represent a cell in a linked list.

Responsibility part: I am able to contain a value and a reference to the cell that follows me. You can access or change my value or my next cell. You can also access the cell at a given index or know my length.

Public API and Key Messages

- value   
- next
- value:
- next:
- length
- at:
- contains:
- how to create instances:

   `Cell withValue: aValue withNext: anotherCell` creates a cell whose value is `aValue`, followed by the cell `anotherCell`
 
Internal Representation and Key Implementation Points.

    Instance Variables
	next:		<Cell> the cell that follows this cell
	value:		<Object> the value contained in this cell


    Implementation Points
"
Class {
	#name : #Cell,
	#superclass : #Object,
	#instVars : [
		'value',
		'next'
	],
	#category : #MyLinkedList
}

{ #category : #'as yet unclassified' }
Cell class >> withValue: anInteger withNext: aCell [
	^ self new value: anInteger; next: aCell; yourself
]

{ #category : #accessing }
Cell >> at: anInteger [
	"returns the cell at index `anInteger`. The first cell (so this cell) is considered to be at index 1. So any index lower than 1 or greater than the length of this chain will throw an error."
	anInteger < 1 ifTrue: [ ^ Error new signal: 'Negative index!' ].
	anInteger = 1 ifTrue: [ ^ self ]
						ifFalse: [ 
							self next ifNil: [ ^ Error new signal: 'Index is too large!' ].
							^ self next at: anInteger - 1 
						]
]

{ #category : #comparing }
Cell >> contains: anInteger [
	"returns true if the chain beginning with this cell contains `anInteger`, false otherwise."
	|res|
	res := self value = anInteger.
	self next ifNotNil: [res := res or: (self next contains: anInteger)].
	^ res
	
]

{ #category : #accessing }
Cell >> length [
	"returns the length of this chain of cells. So basically: it's the the number of cells you can access with `next` before getting nil, added to 1."
	self next ifNil: [ ^ 1 ].
	^ 1 + self next length
]

{ #category : #accessing }
Cell >> next [
	"returns the cell right after this cell."
	^ next
]

{ #category : #accessing }
Cell >> next: aCell [ 
	"sets this cell's next cell to `aCell`"
	next := aCell.
	^ aCell
]

{ #category : #accessing }
Cell >> value [
	"returns the value in this cell."
	^ value
]

{ #category : #accessing }
Cell >> value: anInteger [ 
	"changes the value of this cell to `anInteger`"
	value := anInteger.
	^ anInteger
]
