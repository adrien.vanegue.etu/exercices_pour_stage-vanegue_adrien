"
Class part: I represent a linked list made up of cells.

Responsibility part: I am able to access or remove a cell at a given index. I can also insert a new cell with a given value at a given index. You can access my head and my tail to access my first cell and my last cell.

Public API and Key Messages

- head  
- tail 
- isEmpty
- length
- push:
- pop
- at:
- at: put:
- removeAt:
- contains:
- how to create instances:

   `MyLinkedList initialize` creates an empty list, which is to say a list with a nil head and tail.
 
Internal Representation and Key Implementation Points.

    Instance Variables
	head:		<Cell> the cell that begins this linked list.
	tail:		<Cell> the cell that ends this linked list.
"
Class {
	#name : #MyLinkedList,
	#superclass : #Object,
	#instVars : [
		'head',
		'tail'
	],
	#category : #MyLinkedList
}

{ #category : #initialization }
MyLinkedList class >> initialize [
	^ self new
]

{ #category : #accessing }
MyLinkedList >> at: anInteger [
	"returns the cell in this list at index `anInteger`. This list's head is at index 1 so any index below 1 or greater than this list's length will throw an error."
	self head ifNil: [ ^ Error new signal: 'Empty list!' ].
	^ self head at: anInteger
]

{ #category : #adding }
MyLinkedList >> at: anInteger put: aValue [
	"adds a new cell whose value is `aValue` rigth between the cells at index `anInteger - 1` and `anInteger`. An exception is thrown when `anInteger` is lower than 1 or greater than the length of this list + 1."
	|cellBefore cellAfter newCell|
	anInteger = 1 ifTrue: [ ^ self push:aValue ].
	cellBefore := self at: anInteger - 1.
	cellAfter := cellBefore next.
	newCell := Cell withValue: aValue withNext: cellAfter.
	cellBefore next: newCell.
	cellAfter ifNil: [ tail := newCell ].
	^ newCell
	
]

{ #category : #comparing }
MyLinkedList >> contains: anInteger [ 
	"returns true if this linked list is not empty and the cell chain beginning with this list's head contains `anInteger`"
	^ self head ifNil: [ false ]  ifNotNil: [ self head contains: anInteger ]
]

{ #category : #'accessing - structure variables' }
MyLinkedList >> head [
	"returns this list's head."
	^ head
]

{ #category : #testing }
MyLinkedList >> isEmpty [
	"tells whether this list is empty or not."
	^ self head isNil
]

{ #category : #accessing }
MyLinkedList >> length [
	"returns this list's length, which is the length of the chain of cells beginning with this list's head."
	self head ifNil: [ ^ 0 ].
	^ self head length
]

{ #category : #removing }
MyLinkedList >> pop [
	"removes the first cell from this list and returns this cell that's now unlinked. It throws an error when this list is empty."
	|oldHead|
	self head ifNil: [ ^ Error new signal: 'This list is empty!' ].
	oldHead := self head.
	head := self head next.
	oldHead next ifNil: [ tail :=  nil].
	^ oldHead next: nil; yourself
]

{ #category : #adding }
MyLinkedList >> push: anInteger [ 
	"adds a new cell whose value is `anInteger` at the beginning of this list."
	|newCell oldHead|
	oldHead := self head.
	newCell := Cell withValue: anInteger withNext: oldHead.
	head := newCell.
	oldHead ifNil:
		[ tail := newCell ]
]

{ #category : #removing }
MyLinkedList >> removeAt: anInteger [ 
	"removes the cell at index `anInteger` and returns this cell that is now unlinked. Any index below 1 or greater than the length of this list will throw an exception."
	|cellBefore removedCell|
	anInteger = 1 ifTrue: [ ^ self pop ].
	cellBefore := self at: anInteger - 1.
	removedCell := cellBefore next.
	cellBefore next: removedCell next.
	cellBefore next ifNil: [ tail := cellBefore ].
	^ removedCell next: nil; yourself
]

{ #category : #'accessing - structure variables' }
MyLinkedList >> tail [
	"returns this list's tail."
	^ tail
]
