# Exercices Stage: Adrien Vanègue

Dépôt contenant mes exercices de création de liste chaînée et de génération de "javadoc" pour intégrer l'équipe RMod en tant que stagiaire

## Contenu du dépôt

**Le projet est organisé comme ceci:**

* un package `MyLinkedList` contient mon implémentation de liste chaînée. Il contient les classes `Cell` et `MyLinkedList` qui correspondent respectivement à mes implémentations d'une cellule de liste chaînée ainsi que de la liste chaînée elle-même (encapsulée).

* un package `MyLinkedListTests` contenant les tests des classes du package `MyLinkedList`.

* un package `PharoDoc` contenant la classe servant à générer une documentation pour une classe ou un package Pharo.

*Toutes les classes sont documentées.*

### Exercice Liste chaînée

- La classe `Cell` est utilisée dans la classe `MyLinkedList`, dans sa représentation interne. Cependant l'utilisateur manipulerait directement la classe `MyLinkedList`.

- Les classes sont documentées et utilise des messages assez explicites et courants dans les langages de programmation classique ou dans Pharo-même. Voici cependant un résumé:

    - La classe `Cell` peut être manipulée pour:
        * accéder à la valeur d'une cellule: `value`
        * modifier la valeur d'une cellule: `value:`
        * accéder à la prochaine cellule: `next`
        * modifier le chaînage de la prochaine cellule: `next:`
        * connaître la longueur d'une chaîne de cellules: `length`
        * accéder à la cellule d'indice i dans une chaîne de cellules, commençant à l'indice 1: `at:`
        * tester la présence d'une valeur dans une chaîne de cellules: `contains:`
        * son constructeur `withValue:withNext:` prend en paramètre la valeur de la nouvelle cellule et sa cellule suivante.

    - La classe `MyLinkedList` peut être manipulée pour:
        * accéder à la tête de liste: `head`
        * accéder à la queue de liste: `tail` (pas vraiment exploitée ici. Cela pourraît être utile pour ajouter en tête.)
        * tester si une liste est vide: `isEmpty`
        * connaître la longueur d'une liste: `length`
        * tester la présence d'une valeur dans une liste: `contains:`
        * accéder à l'élément d'indice i: `at:`
        * ajouter un élément en tête: `push: `
        * supprimer un élément en tête: `pop`
        * insérer un élément à l'indice i: `at:put:`
        * supprimer un élément à l'indice i: `removeAt:`
        * son constructeur est `initialize`, qui crée une liste vide.

### Exercice génération de documentation Pharo

- La classe `PharoDoc` dans le package `PharoDoc` ne contient que des méthodes de classe car ne dépendent pas de l'instance.

- Elle contient les méthodes suivantes:
    * `generateFromClass:` génère la documentation d'une classe donnée en paramètre dans un fichier `.txt` du même nom, dans le répertoire `home`.
    * `generateFromClass:inDirectory:` génère cette même documentation dans un répertoire donné en paramètre supplémentaire.
    * `generateFromPackage:` génère la documentation de toutes les classes d'un package donné en paramètre dans un sous-répertoire, du même nom que le package, dans le répertoire `home`. Les documentations des classes du package ont alors le même nom que la classe avec l'extension `.txt`.
    * `generateFromPackage:inDirectory:` génère cette même documentation dans un sous-répertoire du même nom que le package, à la différence que cette méthode permet de choisir le répertoire parent grâce à un paramètre supplémentaire.

- Typiquement: ```PharoDoc generateFromPackage: MyLinkedList package``` génèrerait la documentation des classes `Cell` et `MyLinkedList` du package `MyLinkedList` dans des fichiers `~/MyLinkedList/Cell.txt` et `~/MyLinkedList/MyLinkedList.txt`

## Organisation du travail

- Le premier exercice de liste chaînée a été effectuée en TDD car Pharo s'y prête bien. Cela ne se verra cependant pas forcément dans les commits car j'ai commit à chaque fois qu'un test passait au vert.