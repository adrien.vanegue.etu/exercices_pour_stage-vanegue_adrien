"
Class part:  I am a class able to generate a class' or a package's documentation.

For the Responsibility part: My only responsability is to generate documentation for a class or a package in the directory you want me to.

Class methods:

- generateFromClass:
- generateFromClass:inDirectory:
- generateFromPackage:
- generateFromPackage:inDirectory:
- writeMethodsOf:withStream:
- writeSubclassesOf:withStream:
- writeVariablesOf:withStream:
- writeTitle:withStream:withContent:
- writeTitle:withStream:withContentInOneLine: 
- how to create instances: There's no need to create instances of this class as all its methods are class methods.

You can generate a class' documentation with `generateFromClass:inDirectory:` .
For a package, use `generateFromPackage:inDirectory:`
"
Class {
	#name : #PharoDoc,
	#superclass : #Object,
	#category : #PharoDoc
}

{ #category : #'as yet unclassified' }
PharoDoc class >> generateFromClass: aClass [
	"generate `aClass`'s documentation in a file named 'aClass.txt' in your home folder."
	self generateFromClass: aClass inDirectory: FileLocator home 
]

{ #category : #'as yet unclassified' }
PharoDoc class >> generateFromClass: aClass inDirectory: aDirectory [
	"generate `aClass`' documentation in a file called 'aClass.txt' in `aDirectory`"
	|stream|
	stream := (aDirectory / aClass name, 'txt') asFileReference writeStream.
	self writeTitle: 'Class name: ' withStream: stream withContentInOneLine: aClass name.
	self writeTitle: 'Superclass: ' withStream: stream withContentInOneLine: aClass superclass name.
	self writeSubClassesOf: aClass withStream: stream.
	self writeVariablesOf: aClass withStream: stream.
	self writeMethodsOf: aClass withStream: stream.
	stream close
]

{ #category : #writing }
PharoDoc class >> generateFromPackage: anRPackage [
	"generate `anRPackage`'s documentation in a folder named 'anRPackage' in your home directory."
	self generateFromPackage: anRPackage inDirectory: FileLocator home
]

{ #category : #writing }
PharoDoc class >> generateFromPackage: anRPackage inDirectory: aDirectory [
	"""generate `anRPackage`'s documentation in `aDirectory` / anRPackage"""
	(aDirectory / anRPackage name asString) ensureCreateDirectory.
	anRPackage definedClasses do: [ :each | self generateFromClass: each inDirectory: (aDirectory / anRPackage name)]
]

{ #category : #writing }
PharoDoc class >> writeMethodsOf: aClass withStream: aStream [
	"write all `aClass`'s methods' names from both class side and instance side in `aStream`"
	self writeTitle: 'Methods on class side:' withStream: aStream withContent: aClass class selectors.
	self writeTitle: 'Methods on instance side:' withStream: aStream withContent: aClass selectors
	
]

{ #category : #writing }
PharoDoc class >> writeSubClassesOf: aClass withStream: aStream [
	"write all `aClass`'s subclasses in `aStream`"
	self writeTitle: 'Subclasses:' withStream: aStream withContent: aClass subclasses.
]

{ #category : #writing }
PharoDoc class >> writeTitle: aString withStream: aStream withContent: aCollection [
	"writes an entire section of a class documentation in the stream `aStream`, whose title is `aTitle`. All elements from `aCollection` are written as strings in this stream."
	aStream nextPutAll: aString; cr.
	aCollection do: [ :each | aStream nextPutAll: ('' join: { '- '. each asString }  ); cr].
	aStream cr
]

{ #category : #writing }
PharoDoc class >> writeTitle: aTitle withStream: aStream withContentInOneLine: content [
	"writes an entire section of a class documentation in the stream `aStream`, whose title is `aTitle` and whose content is `content`, which is basically a string."
	aStream nextPutAll: ('' join: {aTitle. content}); cr; cr
]

{ #category : #writing }
PharoDoc class >> writeVariablesOf: aClass withStream: aStream [
	"write all `aClass`'s variables' names from both instance and cass side in `aStream`."
	self writeTitle: 'Class variables:' withStream: aStream withContent: aClass classVariables.
	self writeTitle: 'Instance variables:' withStream: aStream withContent: aClass instanceVariables
]
